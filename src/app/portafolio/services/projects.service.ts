import { Injectable } from '@angular/core';
import { Project } from '../interfaces/project.interface';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  private _projects:Project[] = [
    {
      image: '../../../assets/img/RickMorty.png',
      nombre: 'Rick and Morty App',
      description: 'In this web application you can find your favorite characters from this series by consuming an API',
      techologies: 'Angular, Ionic components, Typescript',
      repo: 'https://github.com/mario8a/UI-RickMorty-API',
      demo: '',
      downloadApp: ''
    },
    {
      image: '../../../assets/img/recomendd.png',
      nombre: 'Recomendd App - Private service transport App',
      description: 'During the development of this app, my team and i implement services with firebase for the real time monitoring for vehicles, payment with stripe and deployment of the Android app in Play Store',
      techologies: 'Angular, Ionic components, Typescript, Firebase',
      repo: '',
      demo: 'https://play.google.com/store/apps/details?id=com.recomendd.recomenddUserNew',
      downloadApp: ''
    },
    {
      image: '../../../assets/img/Journal-app.png',
      nombre: 'Journal App',
      description: 'Journal App is a journal where you can save your great experiences, edit them and upload images',
      techologies: 'React js, Grid CSS, Firebase, Redux',
      repo: 'https://github.com/mario8a/Redux-journalApp',
      demo: 'https://elegant-lamport-2874d0.netlify.app',
      downloadApp: ''
    },
    {
      image: '../../../assets/img/search-letter.png',
      nombre: 'Song Search',
      description: 'On this platform you can search your favorite lyrics song',
      techologies: 'React js, Axios, Flexbox',
      repo: 'https://github.com/mario8a/Lyrics-React',
      demo: 'https://frosty-beaver-f966f5.netlify.app',
      downloadApp: ''
    }
  ];

  get projects():Project[] {
    return this._projects;
  }

  constructor() { }
}
