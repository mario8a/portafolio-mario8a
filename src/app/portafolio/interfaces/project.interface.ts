export interface Project {
   image: string;
   nombre: string;
   description: string;
   techologies: string;
   repo: string;
   demo: string;
   downloadApp?: string;
}