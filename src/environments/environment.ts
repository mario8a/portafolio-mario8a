// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBu-qzQIL0JLDag5O1GYopJaobgQ5xoyM4',
    authDomain: 'mario8a-53cc8.firebaseapp.com',
    projectId: 'mario8a-53cc8',
    storageBucket: 'mario8a-53cc8.appspot.com',
    messagingSenderId: '768838862593',
    appId: '1:768838862593:web:02c9203d0b0797597bbd00'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
