import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../services/projects.service';
import { Project } from '../interfaces/project.interface';

@Component({
  selector: 'app-projet',
  templateUrl: './projet.component.html',
  styleUrls: ['./projet.component.scss']
})
export class ProjetComponent implements OnInit {

  get projetcs():Project[] {
    return this.projectServ.projects
  }

  constructor(private projectServ: ProjectsService) { }

  ngOnInit(): void {
  }

}
