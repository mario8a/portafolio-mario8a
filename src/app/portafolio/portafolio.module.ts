import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainpageComponent } from './mainpage/mainpage.component';
import { HeaderComponent } from './header/header.component';
import { ProjetComponent } from './projet/projet.component';
import { EventsComponent } from './events/events.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [
    MainpageComponent, 
    HeaderComponent, 
    ProjetComponent, 
    EventsComponent, 
    FooterComponent],
  exports: [MainpageComponent],
  imports: [
    CommonModule
  ]
})
export class PortafolioModule { }
